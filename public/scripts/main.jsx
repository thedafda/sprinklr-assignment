import { Component } from 'react';
import $ from 'jQuery';
import _ from 'lodash';
import SpTable from './components/table/table';
import SpSearchBox from './components/searchbar/searchbar';
import SpHeader from './components/header/header';
import mainLess from '../styles/main';


let data = {
	tables: [
		{
			id: 20151,
			name: 'Pirates',
			persons: [
				{
					id: 2015001,
					name: 'Jack Sparrow',
					designation: 'Captain',
					seatPosition: 0
				},
				{
					id: 2015002,
					name: 'Mr. Gibbs',
					designation: 'First Mate',
					seatPosition: 2
				}
			]
		},
		{
			id: 20152,
			name: 'Engineers',
			persons: [
				{
					id: 2015003,
					name: 'Bill',
					designation: 'Project Manager',
					seatPosition: 2
				},
				{
					id: 2015004,
					name: 'John Doe',
					designation: 'Software Engineer',
					seatPosition: 3
				}
			]
		},
		{
			id: 20153,
			name: 'Avengers',
			persons: [
				{
					id: 2015005,
					name: 'Iron Man',
					designation: 'Cool Dude',
					seatPosition: 2
				},
				{
					id: 2015006,
					name: 'Captain America',
					designation: 'Leader',
					seatPosition: 0
				},
				{
					id: 2015007,
					name: 'Hulk',
					designation: 'Smash Smash',
					seatPosition: 3
				}
			]
		}
	]
};


class App extends Component{
	constructor(props) {
		super(props);
		this.state = {
			tables: data.tables
		};
	}

	search(searchText, filtres) {
		this.state.tables.forEach(table =>{
			table.persons.forEach(person =>{
				var name = person.name.toLowerCase();
				var searchTxt = searchText.toLowerCase();
				if(searchText && name.indexOf(searchTxt) > -1){
					person.highlight = true;
					return;
				}
				person.highlight = false;
			});
		});

		this.setState(this.state);
	}

	move(fromSeat, toSeat){
		console.log('From', fromSeat, 'To', toSeat);

		let fromTable = _.find(this.state.tables, o => o.id === fromSeat.tableId );
		let toTable = _.find(this.state.tables, o => o.id === toSeat.tableId );
		let newFrom, newTo;

		if(toSeat.seat.personDetail){
			newFrom = {
				id: toSeat.seat.personDetail.id,
				name: toSeat.seat.personDetail.name,
				designation: toSeat.seat.personDetail.designation,
				highlight: toSeat.seat.personDetail.highlight,
				seatPosition: fromSeat.seat.seatPosition
			};
		}


		if(fromSeat.seat.personDetail){
			newTo = {
				id: fromSeat.seat.personDetail.id,
				name: fromSeat.seat.personDetail.name,
				designation: fromSeat.seat.personDetail.designation,
				highlight: fromSeat.seat.personDetail.highlight,
				seatPosition: toSeat.seat.seatPosition
			};
		}

		_.remove(fromTable.persons, o => o.id === fromSeat.seat.personDetail.id);
		if(toTable.seat && toTable.seat.personDetail && toTable.seat.personDetail.id){
			_.remove(toTable.persons, o => o.id === toTable.seat.personDetail.id);	
		}

		newFrom && fromTable.persons.push(newFrom);
		newTo && toTable.persons.push(newTo);
		
		this.setState(this.state);
		
	}

  	render () {
	  	const keys = Object.keys(this.state.tables);
	  	const SpTablesList = keys.map((key) => {
	  		const table = this.state.tables[key];
	  		return <SpTable tableId={table.id} name={table.name} persons={table.persons} onMove={this.move.bind(this)}></SpTable>;
	  	});

	    return (
	    	<div className='container'>
		    	<SpHeader/>
		    	<div className="mast-head">
		    		<div className="office-name">
		    			Sprikler Office
		    		</div>
		    		<div className="search-box-container">
		    			<SpSearchBox onSearch={this.search.bind(this)}/>
		    		</div>
		    	</div>
		    	<div className='table-container'>
		    		{SpTablesList}
		    	</div>
	    	</div>
	    );
	}
}

$(function(){
	React.render(<App/>, document.body);
});



