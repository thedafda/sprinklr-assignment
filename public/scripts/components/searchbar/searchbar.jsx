import { Component } from 'react';
import _ from 'lodash';

export default class SpSearchBar extends Component{
    constructor(props) {
        super(props);
    }

    handleChange(e){
        var val = this.refs.search.getDOMNode().value;
        this.props.onSearch && this.props.onSearch(val || '');
    }

    render() {
        return (
            <div className='search-bar'>
                <input type="search" ref="search" onChange={_.debounce(this.handleChange.bind(this), 250)}/>
                <div></div>
            </div>
        );
    }
}
