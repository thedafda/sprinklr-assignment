import { Component } from 'react';
import _ from 'lodash';
import SpFlyout from '../flyout/flyout';

export default class SpTable extends Component{
    constructor(props) {
        super(props);

        this.state = {
            persons: props.persons,
            seats: {
                topLeft: {
                    seatPosition: 0,
                    positionClass: 'top-left',
                    showFlyout: false
                },
                topRight: {
                    seatPosition: 1,
                    positionClass: 'top-right',
                    showFlyout: false
                },
                bottomLeft: {
                    seatPosition: 2,
                    positionClass: 'bottom-left',
                    showFlyout: false
                },
                bottomRight: {
                    seatPosition: 3,
                    positionClass: 'bottom-right',
                    showFlyout: false
                }
            }
        };

        this.map = {
            0: 'topLeft',
            1: 'topRight',
            2: 'bottomLeft',
            3: 'bottomRight'
        };

    }

    updateSeats(){
        _.each(this.state.seats, (seat) => {
            seat.personDetail = undefined;
        });

        this.props.persons.forEach((person) => {
            const position = this.map[person.seatPosition];
            if(position){
                this.state.seats[position].personDetail = person;
            }
        }.bind(this));
    }

    handleMouseEnter(seatName, event, id) {
        this.state.seats[seatName].showFlyout = true;
        this.setState(this.state);
    }

    handleMouseOut(seatName, event, id) {
        this.state.seats[seatName].showFlyout = false;
        this.setState(this.state);
    }

    handleDragStart(seat, key, e, id) {
        e.dataTransfer.dropEffect = 'move';
        this.state.seats[key].showFlyout = false;
        this.state.draggedData = seat;
        const fromSeat = {
            tableId: this.props.tableId,
            tableName: this.props.name,
            seat: seat
        };
        e.dataTransfer.setData('text', JSON.stringify(fromSeat));
    }

    handleDragOver(seat, key, e, id) {
        if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
        }
    }

    handleDrop(seat, key, e, id) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        const fromSeat = JSON.parse(e.dataTransfer.getData('text'));
        const toSeat = {
            tableId: this.props.tableId,
            tableName: this.props.name,
            seat: seat
        };
        this.props.onMove && this.props.onMove(fromSeat, toSeat);
    }

    render() {

        this.updateSeats();
        var keys = Object.keys(this.state.seats);

        var seatsEles = keys.map(key => {
            const seat = this.state.seats[key];
            const showFlyout = seat.showFlyout && seat.personDetail && seat.personDetail.id;
            const highlight = seat.personDetail && seat.personDetail.highlight;
            const positionClass = [
                seat.positionClass,
                'seat',
                (seat.personDetail ? 'occupied' : ''),
                (highlight ? 'highlight' : '')
            ].join(' ');
            return <div className={positionClass}
                draggable={seat.personDetail && seat.personDetail.id ? true: false}
                onMouseEnter={this.handleMouseEnter.bind(this, key)}
                onMouseOut={this.handleMouseOut.bind(this, key)}
                onDragStart={this.handleDragStart.bind(this, seat, key)}
                onDragOver={this.handleDragOver.bind(this, seat, key)}
                onDrop={this.handleDrop.bind(this, seat, key)}
                data-person={JSON.stringify(seat.personDetail)}
                >
                {  showFlyout ? <SpFlyout person={seat.personDetail}></SpFlyout> : null }
            </div>
        }.bind(this));

        return (
            <div className='table'>
                {seatsEles}
                <div className='inner-table'>
                    {this.props.name}
                </div>
            </div>
        );
    }
}
