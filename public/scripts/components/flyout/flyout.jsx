import { Component } from 'react';

export default class SpFlyout extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        const person = this.props.person;
        return (
            <div className='flyout-container'>
                <div className='flyout'>
                    <h2>{person.name}</h2>
                    <h4>{person.designation}</h4>
                </div>
            </div>
        );
    }
}
