module.exports = function(grunt) {

	var ExtractTextPlugin = require("extract-text-webpack-plugin");

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
		  build: {
			src: [
			  "public/scripts/lib/lodash.min.js",
			  "public/scripts/lib/jquery.min.js",
			  "public/scripts/lib/angular.min.js",
			  "public/scripts/core/game_of_life.js",
			  "public/scripts/jquery_components/jq_game_of_life.js",
			  "public/scripts/angular_directives/ng_game_of_life.js",
			  "public/scripts/main.js"
			],
			dest: 'dist/main.min.js'
		  }
		},
		cssmin:{
		  build: {
			files: {
			  'dist/styles.min.css': [
				'public/styles/**/*.css'
			  ]
			}
		  }
		},
		copy: {
		  build: {
			files: [
			  {
				expand: true,
				cwd: 'public',
				src: ['imgs/**'],
				dest: 'dist'
			  }
			]
		  }
		},
		processhtml: {
		  build: {
			files: {
			  'dist/index.html': ['public/index.html']
			}
		  }
		},
		jshint: {
		  all: [
			'public/**/*.js',
			'tests/**/*.js',
			'!**/lib/**'
		  ]
		},
		clean:{
		  build: {
			src: ["dist"]
		  },
		  zip: {
			src: [
			  "dist.zip",
			  "project.zip",
			]
		  }
		},
		karma: {
		  unit: {
			configFile: 'karma.conf.js'
		  }
		},
		connect: {
		  dev: {
			options: {
			  port: 9001,
			  base: './public'
			}
		  },
		  dist: {
			options: {
			  port: 9002,
			  base: './dist'
			}
		  }
		},
		compress: {
		  dist: {
			options: {
				archive: 'dist.zip'
			},
			files: [
				{src: ['dist/**/*' , '!./node_modules'], dest: './'}
			]
		  },
		  project: {
			options: {
				archive: 'project.zip'
			},
			files: [
				{src: ['**/*' , '!**/node_modules/**', '!**/dist/**' ,'!dist.zip', '!project.zip'], dest: './'}
			]
		  }
		},
		watch: {
			dev: {
				files: [
					'./public/scripts/**/*.jsx',
					'./public/styles/**/*.less'
				],
				tasks: ['webpack:dev'],
				options: {
					livereload: true,
				}
			}
		},
		webpack: {
	        options: {
	        },
	        dev: {
	        	entry: './public/scripts/main.jsx',
	        	output: {
	        		path: "./public/",
        			filename: "[name].js",
        			chunkFilename: "[id].js",
        			//sourceMapFilename: "[name].[ext].map"
    			},
	        	watch: true,
        		debug: true,
        		devtool: "source-map",
            	module: {
            		loaders: [
            			{
            				test: /\.jsx?$/,
            				exclude: /(node_modules)/,
            				loader: 'babel-loader'
            			},
            			{
			                test: /\.css$/,
			                loader: ExtractTextPlugin.extract("style-loader", "css-loader")
			            },
			            {
			                test: /\.less$/,
			                loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
			            }
            		],
            	},
            	resolve: {
            			// you can now require('file') instead of require('file.js')
            			extensions: ['', '.js', '.jsx', '.json', '.less']
            	},
            	optimize:{
            		minimize: false
            	},
            	externals: {
            		react: 'window.React',
            		jQuery: 'window.jQuery',
            		'$': 'window.jQuery',
            		lodash:  'window._'
            	},
            	plugins: [
        			new ExtractTextPlugin("[name].css")
    			]
	        }
	    }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-webpack');
	grunt.loadNpmTasks('grunt-babel');
	grunt.loadNpmTasks('grunt-contrib-connect');


	grunt.registerTask('build', ['jshint', 'clean', 'uglify', 'cssmin', 'processhtml', 'copy']);
	grunt.registerTask('unit-test', ['jshint', 'karma']);
	grunt.registerTask('default', ['build']);
};